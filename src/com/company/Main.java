package com.company;

public class Main
{

	public static void main(String[] args)
	{
		Summa summa = new Summa();

		for (String str: args)
		{
			new Thread(new Summator(str, summa)::startSummator).start();
		}
	}
}
