package com.company;

/**
 * Created by Roman Taranov on 24.04.2017.
 */
public class Summa
{
	private long summ;

	public Summa()
	{
		this.summ = 0L;
	}

	public long getSumm()
	{
		return summ;
	}

	public void setSumm(long summ)
	{
		this.summ = summ;
	}
}
