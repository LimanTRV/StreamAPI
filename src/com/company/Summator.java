package com.company;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.List;

import static java.nio.file.Files.readAllLines;
import static jdk.nashorn.internal.runtime.ScriptingFunctions.readLine;
import static jdk.nashorn.internal.runtime.ScriptingFunctions.tokenizeString;

/**
 * Created by Roman Taranov on 24.04.2017.
 */
public class Summator
{
	private String fileName;
	private Summa summa;

	public Summator(String fileName, Summa summa)
	{
		this.fileName = fileName;
		this.summa = summa;
	}

	public void startSummator()
	{
		for (String str: getListLineFromFile(fileName))
		{
			ArrayList<Long> listLong = new ArrayList<>();
			for (String subStr: str.split(" "))
			{
				if ((subStr.toCharArray())[0] == '-')
				{
					continue;
				}
				else
				{
					listLong.add(Long.valueOf(subStr));
				}
			}

			summa.setSumm(summa.getSumm() + listLong.stream().reduce(0L, (a, b)-> a + b));
			System.out.println(Thread.currentThread().getName() + ": " + summa.getSumm());
		}
	}

	public List<String> getListLineFromFile(String fileName)
	{
		System.out.println(Thread.currentThread().getName() + ": Read from " + fileName + "-----------------------------------------------------");

		try
		{
			return readAllLines(FileSystems.getDefault().getPath(fileName), Charset.forName("Cp1251"));
		}
		catch (IOException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
}
